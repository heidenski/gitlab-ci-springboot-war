resource "null_resource" "radenda-" {
  triggers {
    cluster_instance_ids = "${join("\n", module.ec2_instance.id)}"
  }

  provisioner "local-exec" {
    working_dir = "./provisioning"
    command = "echo '[webserver-ubuntu:vars] \nansible_ssh_private_key_file = /home/ubuntu/.ssh/id_rsa \n\n[webserver-ubuntu:children] \nwebserver-sekolahlinux \n\n[webserver-sekolahlinux]' > ansible_hosts"
  }

  provisioner "local-exec" {
    working_dir = "./provisioning"
    command = "echo '${join("\n", formatlist("%s ansible_host=%s ansible_port=22 ansible_user=ubuntu", module.ec2_instance.tags_hostname, module.ec2_instance.private_ip))}' >> ansible_hosts"
  }

  provisioner "local-exec" {
    working_dir = "./provisioning"
    command = "echo \"PLEASE WAIT 60s\" && sleep 60 && sh ansible-deploy.sh"
  }

}
